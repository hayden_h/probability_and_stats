"""
@title: Cauchy Process Simulation
@author: Hayden Hohns
@date: 02/02/2019
@brief: A simulation of a Cauchy process on the unit interval using the "ratio-of-normals" method. 
"""

import numpy as np
import matplotlib.pyplot as plt

Delta = 10 ** (-5)
N = 10 ** 5
times = np.squeeze(np.linspace(0.0, N, N) * Delta)
Z = np.random.randn(N) / np.random.randn(N)
Z = Z * Delta
X = np.squeeze(np.cumsum(Z))

plt.figure(figsize = (16, 10), dpi = 120)
plt.plot(times, X)
plt.xlabel("t")
plt.ylabel("X_t")
plt.title("Cauchy Process Generation")
plt.show()