"""
@title: Siegmund's Algorithm
@author: Hayden Hohns
@date: 02/02/2019
@brief: Siegmund's algorithm is  one of the earliest rare-event simulation algorithms. It can be used for the 
estimation of:

l = Prob(tau < infty), 

where tau = inf{n: S_n >= gamma}. Siegmund's method has bounded relative error and is logarithmically efficient. It has 
been used in application to ruin probabilities and queuing theory. 
"""

import numpy as np

mu = -1
N = 10 ** 6
gamma = 13
W = np.zeros(N)

for i in range(0, N):
    S = 0
    while S < gamma:
        X = -mu + np.random.randn(1)[0]
        S = S + X # random walk
    W[i] = np.exp(2 * mu * S)

ellHat = np.mean(W)
relErr = np.std(W) / np.mean(W) / np.sqrt(N)

print("The estimated value for l is: " + str(ellHat))
print("The relative error (in percentage) is: " + str(relErr * 100))